import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public Tnode(String a) {
      setName(a);
   }

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setNextSibling (Tnode p) {
      nextSibling = p;
   }

   public Tnode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild (Tnode a) {
      firstChild = a;
   }

   public Tnode getFirstChild() {
      return firstChild;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();

      if(this.getName() != null) {
         b.append(this.getName());
      }

      if(this.getFirstChild() != null) {
         b.append("(");
         b.append(this.getFirstChild().toString());
         b.append(")");
      }

      if(this.getNextSibling() != null) {
         b.append(",");
         b.append(this.getNextSibling().toString());
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      if (pol.trim().isEmpty()) throw new RuntimeException("Entered string was empty!");

      String[] split = pol.trim().split("\\s+");
      int numbers = 0;
      int operators = 0;

      Stack<Tnode> tnodeStack = new Stack<>();

      for (String s : split) {
         Tnode tnode = new Tnode(s);

         if (doubleCheck(s)){
            tnodeStack.push(tnode);
            numbers++;
         }else{
            if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
               if(tnodeStack.size() < 2)
                  throw new RuntimeException("Fewer than 2 elements in list when trying " +s+ " from expression " +pol + "´while trying to convert to tree!");

               Tnode temp = tnodeStack.pop();
               Tnode temp2 = tnodeStack.pop();

               temp2.setNextSibling(temp); //viitab teisena pop-itule esimenea pop-itu.
               tnode.setFirstChild(temp2); //viitab käesolevale tnode-le firstchild-i.

               tnodeStack.push(tnode);
               operators ++;
            }else{
               throw new RuntimeException("Only operators +,-,*,/ expected, operator " +s+ " was used instead in expression " +pol);
            }

         }
      }

      if(!((operators+1) == numbers)) {
         throw new RuntimeException("Numbers count and operators count is not in balance when interpreting expression: " +pol);
      }

      return tnodeStack.pop();
   }

   public static boolean doubleCheck(String s) {
      try {
         Double.parseDouble(s);
      } catch(NumberFormatException e) {
         return false;
      }
      return true;
   }

   public static void main (String[] param) {
      //String rpn = "1 2 +";
      String rpn = "5 1 - 7 * 6 3 / +";
      //String rpn = "       5        1 -         7        *   6       3 / +          ";
      //String rpn = "";
      //String rpn ="2 +";
      //String rpn = "g";
      //String rpn = "5 1 - 7 * 6 4 3 / + ";
      //String rpn = "1";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res.toString()); // Override siin klassis on see, mis overrideb default toString meetodi! Ja võimaldab kasutada seda, mis me defineerisime.
      // TODO!!! Your tests here
   }
}

